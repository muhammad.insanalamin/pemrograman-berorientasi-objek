# no.1
- Mampu mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat. (Lampirkan link source code terkait).

Program Traveloka menggunakan algoritma pemrograman terstruktur (structured programming) dengan konsep dasar control flow dan modularitas. Konsep ini mempermudah pengembangan dan pemeliharaan program dengan membagi program menjadi beberapa blok yang memiliki fungsi dan tujuan tertentu. Contoh konsep modularitas dapat dilihat pada pembuatan objek-objek seperti Tiket, ReservasiHotel, RentalMobil, Uang, dan MenuTagihan yang masing-masing memiliki fungsionalitas yang disesuaikan dengan kebutuhan program. Sedangkan konsep control flow digunakan dalam pengendalian alur program dengan penggunaan percabangan (if-else) dan pengulangan while.



# no.2 
- Menjelaskan algoritma dari solusi yang dibuat.
[Traveloka.java](/uploads/eee86f170ae2b2c5b4a3870d6cdd28dc/Traveloka.java)


# no.3
- Menjelaskan konsep dasar OOP

Konsep OOP

- Object adalah suatu cara untuk membungkus data dan fungsi bersama menjadi satu unit dalam sebuah program.
- Atribut merupakan identitas data atau informasi dari object yang disebut juga sebagai variable.
- Method adalah tingkah laku yang dilakukan object didalam program.
- Class merupakan blueprint (rancangan) atau prorotipe dari sebuah objek. Class akan mendefinisikan sebuah tipe dari object dimana dapat mendeklarasikan variable dan menciptakan object.

Konsep 4 pilar OOP adalah empat konsep dasar dalam Pemrograman Berorientasi Objek (OOP) yang menjadi dasar bagi pemrograman berorientasi objek.
- Encapsulation : Konsep pengkapsulan memungkinkan kita untuk menyembunyikan detail implementasi dari objek dan hanya menampilkan fungsionalitas yang dapat diakses oleh pengguna. Hal ini dilakukan dengan membatasi akses ke properti dan metode objek dengan menggunakan access modifiers seperti public, private, dan protected. Encapsulation merupakan proses di mana sebuah penanganan data ditempatkan di dalam wadah tunggal yang disebut sebagai class. Dengan begini, kita cukup menggunakan data tersebut tanpa harus tahu bagaimana proses yang terjadi sampai data tersebut bisa digunakan.

- Inheritance (Pewarisan) : Konsep pewarisan memungkinkan kita untuk membuat kelas baru dengan memanfaatkan sifat-sifat yang sudah didefinisikan dalam kelas yang sudah ada. Dengan cara ini, kita dapat menghemat waktu dalam penulisan kode dan menghindari duplikasi kode.

- Polymorphism (Banyak Bentuk) Konsep polimorfisme memungkinkan kita untuk menggunakan metode yang sama pada objek yang berbeda-beda. Dalam hal ini, satu metode bisa memiliki implementasi yang berbeda-beda pada setiap objek, tergantung pada kebutuhan yang ada.

- Abstraction : Konsep abstraksi memungkinkan kita untuk membuat objek yang lebih kompleks dengan cara membaginya menjadi objek yang lebih sederhana dan lebih mudah dimengerti. Hal ini dilakukan dengan cara menyederhanakan dan mengkapsulkan fitur objek sehingga kemudian dapat diakses atau digunakan dengan lebih mudah.

Keempat konsep ini bersama-sama membentuk dasar dari pemrograman berorientasi objek dan menjadi landasan dalam membuat aplikasi yang lebih kompleks.

# no.4
Dibawah ini merupakan pengimplementasian Encapsulation, untuk atribut yang dibuat menggunakan modifier **private** yang tidak bisa diakses diluar class.
[Pulsa.java](/uploads/cdb7865efc0d84197a943b7cac44b876/Pulsa.java)
# no.5
Dibawah ini merupakan pengimplementasian Abstraction diibaratkan class abstrak merupakan sebuah wadah dan turunan dari class abstrak merupakan isinya
atribut dan method yang ada pada abstrak harus ada di class turunan/sub class.
[Tagihan.java](/uploads/3854f106861f0de173b57a2fc7690f74/Tagihan.java)[BPJS.java](/uploads/2b63cacc9684c10a20a7eb01ca58b23a/BPJS.java)[PLN.java](/uploads/96d2a5d00)[Pulsa.java](/uploads/d47b204ff6f33fdb97f92192ccfb046d/Pulsa.java)

# no.6
Dibawah ini merupakan pengimplementasian Polymorphism
[Uang.java](/uploads/fa605b5c964da4be5e98d7ea2153d94c/Uang.java)
[Uangku.java](/uploads/3ab1c6d281962f2c9794c2c86e598aa3/Uangku.java)

Inheritance
Dibawah ini merupakan pengimplementasian Inheritance yang dimana  menggunakan inheritance lebih efisien dan menghindari duplikasi code/codingan
[Tiket.java](/uploads/7e1ef91bac3351468fff2c22b2dc2d2e/Tiket.java)[TiketHotel.java](/uploads/9f202fd6a30940f4b9366f8942cf70d2/TiketHotel.java)
[TiketKeretaApi.java](/uploads/46afc4a9140c771dd5cf450837c30b7d/TiketKeretaApi.java)
[TiketPesawat.java](/uploads/29a4148659c259fc71e9d4b5125b3483/TiketPesawat.java)[ReservasiHotel.java](/uploads/167d829ad8462e0e1d7b581ddb2eeb73/ReservasiHotel.java)
[TiketBus.java](/uploads/653f67f90a84c52be1bee2dbcad5f239/TiketBus.java)
# no.7
1. Use case Login : untuk mengotentikasi pengguna agar dapat melakukan login pada aplikasi. Diimplementasikan pada class Login
2. Use case Memesan Tiket : untuk merepresentasikan tiket yang dibeli oleh pengguna. Diimplementasikan pada class Tiket,Tiket Pesawat, Bus dan Kereta Api.
3. Use case reservasi hotel : untuk merepresentasikan proses reservasi hotel. Diimplementasikan pada class TiketHotal dan class Resevasi hotel
4. Use Case Memesanan mobil : untuk merepresentasikan proses rental mobil. Diimplementasikan pada class RentalMobil
7. Use Case Cek Saldo : untuk merepresentasikan jumlah uang pada akun pengguna dan merepresentasikan dompet digital pengguna. Diimplementasikan pada class Uang dan class Uangku
9. Use Case Membayar Tagihan: untuk menampilkan informasi tagihan yang harus dibayar oleh pengguna. Diimplementasikan pada class Tagihan, class BPJS, class Pulsa, dan Class PLN.

# no.8
 Class Diagram
![image](https://gitlab.com/cindyoktavian/pemrograman-berorientasi-objek/-/raw/main/Image/Class_Diagram.drawio.png)
Use Case
![image](https://gitlab.com/cindyoktavian/pemrograman-berorientasi-objek/-/raw/main/Image/Screenshots39.jpg)

# no.9
Link yt : https://youtu.be/wj7dSMFSUkY

# no.10 
![image](https://gitlab.com/cindyoktavian/pemrograman-berorientasi-objek/-/raw/main/Image/Image1.jpg)
![image](https://gitlab.com/cindyoktavian/pemrograman-berorientasi-objek/-/raw/main/Image/Image2.jpg)
![image](https://gitlab.com/cindyoktavian/pemrograman-berorientasi-objek/-/raw/main/Image/Image3.jpg)
![image](https://gitlab.com/cindyoktavian/pemrograman-berorientasi-objek/-/raw/main/Image/Image5.jpg)
![image](https://gitlab.com/cindyoktavian/pemrograman-berorientasi-objek/-/raw/main/Image/Image4.jpg)













